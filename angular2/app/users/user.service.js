"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var User_1 = require('../models/User');
var core_1 = require('@angular/core');
var auth_service_1 = require('../auth0/auth.service');
var auth_config_1 = require('../auth0/auth.config');
var http_1 = require("@angular/http");
var recordCount = 5;
var UserService = (function () {
    function UserService(auth, http) {
        this.auth = auth;
        this.http = http;
        this.page = 0;
        this.total = 0;
        this.length = 0;
        this.previousQuery = null;
    }
    UserService.prototype.getUsers = function (name) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var query = '';
            if (name && name.length > 0) {
                query = "(_exists_:user_metadata.name AND user_metadata.name:" + name + "*) OR (_missing_:user_metadata.name AND (name:" + name + "* OR username:" + name + "* OR nickname:" + name + "* OR given_name:" + name + "* OR family_name:" + name + "*))";
            }
            _this.search(query, 0, function (data, error) {
                if (error) {
                    reject(error);
                }
                else {
                    _this.previousQuery = query;
                    _this.page = 0;
                    _this.length = 0;
                    _this.total = data ? data.total : 0;
                    resolve(_this.parseSearchResult(data));
                }
            });
        });
    };
    UserService.prototype.getMoreUsers = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            if (!_this.isCanMore()) {
                resolve(null);
            }
            ;
            _this.search(_this.previousQuery, _this.page + 1, function (data, error) {
                if (error) {
                    reject(error);
                }
                else {
                    _this.page++;
                    resolve(_this.parseSearchResult(data));
                }
            });
        });
    };
    UserService.prototype.parseSearchResult = function (searchResult) {
        if (searchResult) {
            this.length += searchResult.length;
            if (searchResult.users) {
                var result = [];
                for (var _i = 0, _a = searchResult.users; _i < _a.length; _i++) {
                    var item = _a[_i];
                    result.push(this.getUserFromProfile(item));
                }
                return result;
            }
        }
        return null;
    };
    UserService.prototype.getUser = function (id) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.get("https://" + auth_config_1.myConfig.webtaskDomain + "/users/" + id + "?webtask_no_cache=1")
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(_this.getUserFromProfile(data));
            }, function (error) { return reject(error); });
        });
    };
    UserService.prototype.isCanMore = function () {
        return this.length < this.total;
    };
    UserService.prototype.search = function (query, page, cb) {
        this.http.get("https://" + auth_config_1.myConfig.webtaskDomain + "/users?webtask_no_cache=1&per_page=" + recordCount + "&page=" + page + "&include_totals=true&search_engine:'v2'&q=" + query)
            .map(function (res) { return res.json(); })
            .subscribe(function (data) { return cb(data, null); }, function (error) { return cb(null, error); });
    };
    UserService.prototype.exists = function (email, nickname, ignorid) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var query = null;
            if (email) {
                query = "email:\"" + email + "\"";
            }
            else if (nickname) {
                query = "(!user_id.raw:" + ignorid + " AND _exists_:user_metadata.nickname AND user_metadata.nickname:" + nickname + ") OR (_missing_:user_metadata.nickname AND nickname:" + nickname + ")";
            }
            if (!query) {
                resolve(false);
            }
            _this.search(query, 0, function (data, error) {
                if (error) {
                    reject(error);
                }
                else {
                    var result = false;
                    if (data && data.users) {
                        for (var _i = 0, _a = data.users; _i < _a.length; _i++) {
                            var item = _a[_i];
                            var user = _this.getUserFromProfile(item);
                            if ((email && user.email && email.toLowerCase() === user.email.toLowerCase())
                                || (nickname && user.nickname && nickname.toLowerCase() === user.nickname.toLowerCase())) {
                                result = true;
                                break;
                            }
                        }
                    }
                    resolve(result);
                }
            });
        });
    };
    UserService.prototype.update = function (id, userInfo) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.post("https://" + auth_config_1.myConfig.webtaskDomain + "/users/" + id + "?webtask_no_cache=1", userInfo)
                .map(function (res) { return res.json(); })
                .subscribe(function (data) { return resolve(data); }, function (error) { return reject(error); });
        });
    };
    UserService.prototype.getUserFromProfile = function (profile) {
        var result = new User_1.User();
        result.id = profile.user_id;
        result.name = profile.user_metadata ? profile.user_metadata.name || profile.name : profile.name;
        result.avatar = profile.user_metadata ? profile.user_metadata.picture || profile.picture : profile.picture;
        result.email = profile.email;
        result.startDate = new Date(profile.user_metadata ? profile.user_metadata.created_at || profile.created_at : profile.created_at);
        result.nickname = profile.user_metadata ? profile.user_metadata.nickname || profile.nickname : profile.nickname;
        return result;
    };
    UserService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [auth_service_1.Auth, http_1.Http])
    ], UserService);
    return UserService;
}());
exports.UserService = UserService;
//# sourceMappingURL=user.service.js.map