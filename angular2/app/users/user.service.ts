import { User } from '../models/User';
import { Injectable } from '@angular/core';
import { Auth } from '../auth0/auth.service';
import { myConfig } from '../auth0/auth.config';
import { Http } from "@angular/http";

const recordCount: Number = 5;

@Injectable()
export class UserService {
  page: number = 0;
  total: Number = 0;
  length: Number = 0;
  previousQuery: String = null;

  constructor(private auth: Auth, private http: Http) { }

  getUsers(name: string): Promise<User[]> {
    return new Promise((resolve, reject) => {
      var query = ''

      if (name && name.length > 0) {
        query = `(_exists_:user_metadata.name AND user_metadata.name:${name}*) OR (_missing_:user_metadata.name AND (name:${name}* OR username:${name}* OR nickname:${name}* OR given_name:${name}* OR family_name:${name}*))`;
      }

      this.search(query, 0, (data: any, error: any) => {
        if (error) {
          reject(error);
        } else {
          this.previousQuery = query;
          this.page = 0;
          this.length = 0;
          this.total = data ? data.total : 0;

          resolve(this.parseSearchResult(data));
        }
      });
    });
  }

  getMoreUsers(): Promise<User[]> {
    return new Promise((resolve, reject) => {
      if (!this.isCanMore()) {
        resolve(null);
      };

      this.search(this.previousQuery, this.page + 1, (data: any, error: any) => {
        if (error) {
          reject(error);
        } else {
          this.page++;
          resolve(this.parseSearchResult(data));
        }
      });
    });
  }

  private parseSearchResult(searchResult: any): User[] {
    if (searchResult) {
      this.length += searchResult.length;

      if (searchResult.users) {
        const result: User[] = [];

        for (let item of searchResult.users) {
          result.push(this.getUserFromProfile(item));
        }

        return result;
      }
    }

    return null;
  }

  getUser(id: string): Promise<User> {
    return new Promise((resolve, reject) => {
      this.http.get(`https://${myConfig.webtaskDomain}/users/${id}?webtask_no_cache=1`)
        .map(res => res.json())
        .subscribe(data => {
          resolve(this.getUserFromProfile(data));
        }, error => reject(error));
    });
  }

  isCanMore(): boolean {
    return this.length < this.total;
  }

  search(query, page: number, cb: (data: any, error: any) => any): void {
    this.http.get(`https://${myConfig.webtaskDomain}/users?webtask_no_cache=1&per_page=${recordCount}&page=${page}&include_totals=true&search_engine:'v2'&q=${query}`)
      .map(res => res.json())
      .subscribe(data => cb(data, null), error => cb(null, error));
  }

  exists(email: string, nickname: string, ignorid: string): Promise<boolean> {
    return new Promise((resolve, reject) => {
      var query: string = null;

      if (email) {
        query = `email:"${email}"`;
      } else if (nickname) {
        query = `(!user_id.raw:${ignorid} AND _exists_:user_metadata.nickname AND user_metadata.nickname:${nickname}) OR (_missing_:user_metadata.nickname AND nickname:${nickname})`;
      }

      if (!query) {
        resolve(false);
      }

      this.search(query, 0, (data: any, error: any) => {
        if (error) {
          reject(error);
        } else {
          var result = false;

          if (data && data.users) {
            for (let item of data.users) {
              const user: User = this.getUserFromProfile(item);

              if ((email && user.email && email.toLowerCase() === user.email.toLowerCase())
                || (nickname && user.nickname && nickname.toLowerCase() === user.nickname.toLowerCase())) {
                result = true;
                break;
              }
            }
          }

          resolve(result);
        }
      })
    });
  }

  update(id: string, userInfo): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.post(`https://${myConfig.webtaskDomain}/users/${id}?webtask_no_cache=1`, userInfo)
        .map(res => res.json())
        .subscribe(data => resolve(data), error => reject(error));
    });
  }

  private getUserFromProfile(profile): User {
    const result = new User();
    result.id = profile.user_id;
    result.name = profile.user_metadata ? profile.user_metadata.name || profile.name : profile.name;
    result.avatar = profile.user_metadata ? profile.user_metadata.picture || profile.picture : profile.picture;
    result.email = profile.email;
    result.startDate = new Date(profile.user_metadata ? profile.user_metadata.created_at || profile.created_at : profile.created_at);
    result.nickname = profile.user_metadata ? profile.user_metadata.nickname || profile.nickname : profile.nickname;
    return result;
  }
}