import { Component, OnInit } from '@angular/core';
import { User } from '../models/user';
import { Auth } from '../auth0/auth.service';
import { UserService } from '../users/user.service';
import { LoadingManager } from '../loading/loadingManager';

@Component({
  moduleId: module.id,
  selector: 'users',
  templateUrl: 'users.component.html'
})

export class UsersComponent extends LoadingManager implements OnInit {
  users: User[] = null;

  constructor(
    private auth: Auth,
    private userService: UserService
  ) {
    super(false);
  }

  ngOnInit(): void {
    if (!this.auth.authenticated) {
      this.auth.logout();
    }

    this.search(null);
  }

  search(text): void {
    this.standby();
    this.userService.getUsers(text)
      .then(users => {
        this.ready();
        this.users = users;
      })
      .catch(error => {
        this.ready();
        alert('search users error: ' + error)
      });
  }

  more(): void {
    if (this.userService.isCanMore) {
      this.userService.getMoreUsers()
        .then(users => {
          if (users) {
            if (this.users) {
              this.users = this.users.concat(users);
            } else {
              this.users = users;
            }
          }
        })
        .catch(error => alert('search users error: ' + error));
    }
  }
}