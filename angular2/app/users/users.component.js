"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var auth_service_1 = require('../auth0/auth.service');
var user_service_1 = require('../users/user.service');
var loadingManager_1 = require('../loading/loadingManager');
var UsersComponent = (function (_super) {
    __extends(UsersComponent, _super);
    function UsersComponent(auth, userService) {
        _super.call(this, false);
        this.auth = auth;
        this.userService = userService;
        this.users = null;
    }
    UsersComponent.prototype.ngOnInit = function () {
        if (!this.auth.authenticated) {
            this.auth.logout();
        }
        this.search(null);
    };
    UsersComponent.prototype.search = function (text) {
        var _this = this;
        this.standby();
        this.userService.getUsers(text)
            .then(function (users) {
            _this.ready();
            _this.users = users;
        })
            .catch(function (error) {
            _this.ready();
            alert('search users error: ' + error);
        });
    };
    UsersComponent.prototype.more = function () {
        var _this = this;
        if (this.userService.isCanMore) {
            this.userService.getMoreUsers()
                .then(function (users) {
                if (users) {
                    if (_this.users) {
                        _this.users = _this.users.concat(users);
                    }
                    else {
                        _this.users = users;
                    }
                }
            })
                .catch(function (error) { return alert('search users error: ' + error); });
        }
    };
    UsersComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'users',
            templateUrl: 'users.component.html'
        }), 
        __metadata('design:paramtypes', [auth_service_1.Auth, user_service_1.UserService])
    ], UsersComponent);
    return UsersComponent;
}(loadingManager_1.LoadingManager));
exports.UsersComponent = UsersComponent;
//# sourceMappingURL=users.component.js.map