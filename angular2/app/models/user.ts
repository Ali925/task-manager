export class User {
  id: string;
  name: string;
  email: string;
  avatar: string;
  startDate: Date;
  nickname: string;
}