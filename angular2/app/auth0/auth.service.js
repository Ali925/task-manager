"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var angular2_jwt_1 = require('angular2-jwt');
var router_1 = require('@angular/router');
var auth_config_1 = require('./auth.config');
var angular2_jwt_2 = require('angular2-jwt');
require('rxjs/add/operator/map');
var Auth = (function () {
    function Auth(router, authHttp) {
        var _this = this;
        this.router = router;
        this.authHttp = authHttp;
        // Configure Auth0
        this.auth0 = new Auth0({
            domain: auth_config_1.myConfig.domain,
            clientID: auth_config_1.myConfig.clientID,
            callbackOnLocationHash: true,
            callbackURL: auth_config_1.myConfig.callbackURL
        });
        var result = this.auth0.parseHash(window.location.hash);
        if (result) {
            if (result.error) {
                alert('error: ' + result.error);
                this.logout();
            }
            else {
                localStorage.setItem('id_token', result.idToken);
                this.auth0.getProfile(result.idToken, function (error, profile) {
                    if (error) {
                        alert('Sign in error: ' + error);
                        _this.logout();
                    }
                    else {
                        localStorage.setItem('user_id', profile.user_id);
                    }
                });
            }
        }
    }
    Auth.prototype.signUp = function (name, email, passcode, nickname, cb) {
        this.auth0.signup({
            connection: 'Username-Password-Authentication',
            responseType: 'token',
            email: email,
            password: passcode,
            user_metadata: {
                name: name,
                nickname: nickname
            }
        }, cb);
    };
    ;
    Auth.prototype.login = function (username, password, cb) {
        this.auth0.login({
            connection: 'Username-Password-Authentication',
            responseType: 'token',
            email: username,
            password: password
        }, cb);
    };
    ;
    Auth.prototype.googleLogin = function () {
        this.auth0.login({
            connection: 'google-oauth2'
        }, function (err) {
            if (err)
                alert("something went wrong: " + err.message);
        });
    };
    ;
    Auth.prototype.authenticated = function () {
        // Check if there's an unexpired JWT
        // It searches for an item in localStorage with key == 'id_token'
        return angular2_jwt_1.tokenNotExpired();
    };
    ;
    Auth.prototype.logout = function () {
        // Remove token from localStorage
        localStorage.removeItem('id_token');
        localStorage.removeItem('user_id');
        this.router.navigate(['/']);
    };
    ;
    Auth.prototype.isCurrentUser = function (id) {
        return id === localStorage.getItem('user_id');
    };
    Auth = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [router_1.Router, angular2_jwt_2.AuthHttp])
    ], Auth);
    return Auth;
}());
exports.Auth = Auth;
//# sourceMappingURL=auth.service.js.map