import { Injectable } from '@angular/core';
import { tokenNotExpired } from 'angular2-jwt';
import { Router } from '@angular/router';
import { myConfig } from './auth.config';
import { AuthHttp } from 'angular2-jwt';
import 'rxjs/add/operator/map';

// Avoid name not found warnings
declare var Auth0: any;

@Injectable()
export class Auth {
  // Configure Auth0
  auth0 = new Auth0({
    domain: myConfig.domain,
    clientID: myConfig.clientID,
    callbackOnLocationHash: true,
    callbackURL: myConfig.callbackURL
  });

  constructor(private router: Router, private authHttp: AuthHttp) {
    var result = this.auth0.parseHash(window.location.hash);

    if (result) {
      if (result.error) {
        alert('error: ' + result.error);
        this.logout();
      } else {
        localStorage.setItem('id_token', result.idToken);
        this.auth0.getProfile(result.idToken, (error, profile) => {
          if (error) {
            alert('Sign in error: ' + error);
            this.logout();
          } else {
            localStorage.setItem('user_id', profile.user_id);
          }
        });
      }
    }
  }

  public signUp(name: string, email: string, passcode: string, nickname: string, cb: (error: any) => void): void {
    this.auth0.signup({
      connection: 'Username-Password-Authentication',
      responseType: 'token',
      email: email,
      password: passcode,
      user_metadata: {
        name: name,
        nickname: nickname
      }
    }, cb);
  };

  public login(username, password, cb: (error: any) => void): void {
    this.auth0.login({
      connection: 'Username-Password-Authentication',
      responseType: 'token',
      email: username,
      password: password
    }, cb);
  };

  public googleLogin() {
    this.auth0.login({
      connection: 'google-oauth2'
    }, function (err) {
      if (err) alert("something went wrong: " + err.message);
    });
  };

  public authenticated(): boolean {
    // Check if there's an unexpired JWT
    // It searches for an item in localStorage with key == 'id_token'
    return tokenNotExpired();
  };

  public logout(): void {
    // Remove token from localStorage
    localStorage.removeItem('id_token');
    localStorage.removeItem('user_id');
    this.router.navigate(['/']);
  };

  public isCurrentUser(id: string): boolean {
    return id === localStorage.getItem('user_id');
  }
}