interface AuthConfiguration {
    clientID: string,
    domain: string,
    callbackURL: string,
    webtaskDomain: string
}

export const myConfig: AuthConfiguration = {
    clientID: 'PbGRlfKRw58GbtDG8WMnJjXxlVDw5SGU',
    domain: 'testaccountname.auth0.com',
    callbackURL: 'http://localhost:3000/users',
    webtaskDomain: 'testaccountname.us.webtask.io/webtask'
};