var app = new (require('express'))();
var wt = require('webtask-tools');
var jwt = require('jsonwebtoken');
var crypto = require('crypto');

var auth0Config = {
    credentials: {
        globalClientSecret: 'g7xXVmQJn52cpKvpU7XOTbbzsc-aFTBD1FL8Nnc8trSHsSnRuvmJ7Qhg7EeRsaYE',
        apiKey: '1rKHaTREsUMt3wNaJiFuaGg7bBkX1xhm'
    },
    domain: 'testaccountname.auth0.com',
    lifetimeInSeconds: 31104000,
    payload: {
        scopes: {
            users: {
                actions: [
                    "read",
                    "update",
                    "delete",
                    "create"
                ]
            }
        },
        iat: Math.floor(Date.now() / 1000)
    }
};

auth0Config.payload.jti = crypto.createHash('md5')
    .update(JSON.stringify(auth0Config.payload))
    .digest('hex');
auth0Config.token = jwt.sign(auth0Config.payload,
    new Buffer(auth0Config.credentials.globalClientSecret, 'base64').toString('binary'),
    {
        audience: auth0Config.credentials.apiKey,
        expiresIn: auth0Config.lifetimeInSeconds,
        noTimestamp: true
    });

var management = new (require('auth0@2.4.0').ManagementClient)({
    token: auth0Config.token,
    domain: auth0Config.domain
});

app.get('/users', function(req, res) {
    management.users.getAll(req.webtaskContext.query).then(users => {
        res.send(users);
    }).catch(error => {
        res.send(error);
    });
});

// app.post('/users', function (req, res) {
//     management.users.create(req.webtaskContext.body).then(user => {
//         res.send(user);
//     }).catch(error => {
//         res.send(error);
//     });
// });

app.post('/users/:id', function(req, res) {
    management.users.updateUserMetadata({ id: req.params.id }, req.webtaskContext.body).then(user => {
        res.send(user);
    }).catch(error => {
        res.send(error);
    });
});

app.get('/users/:id', function(req, res) {
    management.users.get({ id: req.params.id }).then(user => {
        res.send(user);
    }).catch(error => {
        res.send(error);
    });
});

module.exports = wt.fromExpress(app);