import { Component } from '@angular/core';
import { UserService } from '../users/user.service';
import { Location } from '@angular/common';
import { Auth } from '../auth0/auth.service';
import { LoadingManager } from '../loading/loadingManager';

@Component({
  moduleId: module.id,
  selector: 'registration',
  templateUrl: 'registration.template.html'
})

export class RegistrationComponent extends LoadingManager {
  constructor(
    private auth: Auth,
    private userService: UserService,
    private location: Location
  ) {
    super(false);
  }

  signUp(name: string, email: string, passcode: string, confirmPasscode: string, nickname: string): void {
    if (!name || name.length == 0) {
      alert('Please enter name');
      return;
    }

    if (!email || email.length == 0) {
      alert('Please enter email');
      return;
    }

    if (!passcode || passcode.length == 0) {
      alert('Please enter passcode');
      return;
    }

    if (!confirmPasscode || confirmPasscode.length == 0) {
      alert('Please confirm passcode');
      return;
    }

    if (passcode !== confirmPasscode) {
      alert('Passcode does not match the confirm passcode');
      return;
    }

    if (!nickname || nickname.length == 0) {
      alert('Please enter nickname');
      return;
    }

    this.standby();
    this.userService.exists(email, null, '').then((result: boolean) => {
      if (result) {
        this.displayAlert('The user already exists.', null);
      } else {
        this.userService.exists(null, nickname, '').then((result: boolean) => {
          if (result) {
            this.displayAlert('The nickname already exists.', null);
          } else {
            this.auth.signUp(name, email, passcode, nickname, (error => {
              this.ready();

              if (error) {
                this.displayAlert(null, error.message);
              }
            }));
          }
        }).catch((error) => this.displayAlert(null, error));
      }
    }).catch((error) => this.displayAlert(null, error));
  }

  goBack(): void {
    this.location.back();
  }

  private displayAlert(message: string, error: any) {
    this.ready();

    if (message) {
      alert(message);
    } else if (error) {
      alert('sign up error: ' + error);
    }
  }
};