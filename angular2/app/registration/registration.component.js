"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var user_service_1 = require('../users/user.service');
var common_1 = require('@angular/common');
var auth_service_1 = require('../auth0/auth.service');
var loadingManager_1 = require('../loading/loadingManager');
var RegistrationComponent = (function (_super) {
    __extends(RegistrationComponent, _super);
    function RegistrationComponent(auth, userService, location) {
        _super.call(this, false);
        this.auth = auth;
        this.userService = userService;
        this.location = location;
    }
    RegistrationComponent.prototype.signUp = function (name, email, passcode, confirmPasscode, nickname) {
        var _this = this;
        if (!name || name.length == 0) {
            alert('Please enter name');
            return;
        }
        if (!email || email.length == 0) {
            alert('Please enter email');
            return;
        }
        if (!passcode || passcode.length == 0) {
            alert('Please enter passcode');
            return;
        }
        if (!confirmPasscode || confirmPasscode.length == 0) {
            alert('Please confirm passcode');
            return;
        }
        if (passcode !== confirmPasscode) {
            alert('Passcode does not match the confirm passcode');
            return;
        }
        if (!nickname || nickname.length == 0) {
            alert('Please enter nickname');
            return;
        }
        this.standby();
        this.userService.exists(email, null, '').then(function (result) {
            if (result) {
                _this.displayAlert('The user already exists.', null);
            }
            else {
                _this.userService.exists(null, nickname, '').then(function (result) {
                    if (result) {
                        _this.displayAlert('The nickname already exists.', null);
                    }
                    else {
                        _this.auth.signUp(name, email, passcode, nickname, (function (error) {
                            _this.ready();
                            if (error) {
                                _this.displayAlert(null, error.message);
                            }
                        }));
                    }
                }).catch(function (error) { return _this.displayAlert(null, error); });
            }
        }).catch(function (error) { return _this.displayAlert(null, error); });
    };
    RegistrationComponent.prototype.goBack = function () {
        this.location.back();
    };
    RegistrationComponent.prototype.displayAlert = function (message, error) {
        this.ready();
        if (message) {
            alert(message);
        }
        else if (error) {
            alert('sign up error: ' + error);
        }
    };
    RegistrationComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'registration',
            templateUrl: 'registration.template.html'
        }), 
        __metadata('design:paramtypes', [auth_service_1.Auth, user_service_1.UserService, common_1.Location])
    ], RegistrationComponent);
    return RegistrationComponent;
}(loadingManager_1.LoadingManager));
exports.RegistrationComponent = RegistrationComponent;
;
//# sourceMappingURL=registration.component.js.map