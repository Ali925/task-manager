import { Component, OnInit } from '@angular/core';
import { User } from '../models/user';
import { Location } from '@angular/common';
import { UserService } from '../users/user.service';
import { ActivatedRoute, Params } from '@angular/router';
import { Auth } from '../auth0/auth.service';

const MAX_AVATAR_SZIE: Number = 102400;

@Component({
  moduleId: module.id,
  selector: 'profile',
  templateUrl: 'profile.component.html'
})

export class ProfileComponent implements OnInit {
  user: User = null;
  isReadonly: boolean = true;
  startDate: string = null;
  isActiveName = '';
  isActiveDate = '';
  isActiveNick = '';

  constructor(
    private auth: Auth,
    private userService: UserService,
    private route: ActivatedRoute,
    private location: Location
  ) {
    this.isActiveName = '';
    this.isActiveDate = '';
    this.isActiveNick = '';
  }

  ngOnInit(): void {
    this.route.params.forEach((params: Params) => {
      this.userService.getUser(params['id']).then(user => {
        this.user = user;
        this.startDate = this.dateToString(this.user.startDate);
        this.isReadonly = !this.auth.isCurrentUser(this.user.id);
      }).catch(error => alert('get user error: ' + error));
    });
  }

  goBack(): void {
    this.location.back();
  }

  onBlur(owner: string): void {
    if (this.isReadonly) {
      return;
    }

    switch (owner) {
      case 'name':
        this.userService.update(this.user.id, { name: this.user.name })
          .catch(error => alert('update profile error: ' + error));
        break;
      case 'startDate':
        this.userService.update(this.user.id, { created_at: this.user.startDate.toISOString() })
          .catch(error => alert('update profile error: ' + error));
        break;
      case 'nickname':
        this.userService.exists(null, this.user.nickname, this.user.id).then((result: boolean) => {
          console.log(result);
          if (!result) {
            this.userService.update(this.user.id, { nickname: this.user.nickname })
              .catch(error => alert('update profile error: ' + error));
          }
        }).catch((error) => alert('sign up error: ' + error));
        break;
    }
  }

  onChange(event): void {
    if (this.isReadonly) {
      return;
    }

    const files = event.srcElement.files;

    if (!files) {
      return;
    }

    for (let file of files) {
      if (file.size >= MAX_AVATAR_SZIE) {
        alert('Avatars size is very large, it must be less than 100kb.');
        continue;
      }

      const reader = new FileReader();
      reader.onload = data => {
        const contents: any = data.target;
        this.user.avatar = contents.result;
        this.userService.update(this.user.id, { picture: this.user.avatar })
          .catch(error => { alert('update profile error: ' + error) });
      };
      reader.readAsDataURL(file);
    };
  }

  onAvatarClick(input: HTMLElement): void {
    if (!this.isReadonly) {
      input.click();
    }
  }

  onStartDateChange(): void {
    this.user.startDate = new Date(this.startDate);
  }

  private dateToString(date: Date): string {
    return date.getFullYear() + '-' + ('0' + (date.getMonth() + 1)).slice(-2) + '-' + ('0' + date.getDate()).slice(-2);
  }

  checkActiveClass(className) {
    switch (className) {
      case 'name':
        this.isActiveName = 'activeTr';
        break;
      case 'date':
        this.isActiveDate = 'activeTr';
        break;
      case 'nick':
        this.isActiveNick = 'activeTr';
    }
  }

  checkDeactiveClass(className) {
    switch (className) {
      case 'name':
        this.isActiveName = '';
        break;
      case 'date':
        this.isActiveDate = '';
        break;
      case 'nick':
        this.isActiveNick = '';
    }
  }
}