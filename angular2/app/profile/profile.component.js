"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var common_1 = require('@angular/common');
var user_service_1 = require('../users/user.service');
var router_1 = require('@angular/router');
var auth_service_1 = require('../auth0/auth.service');
var MAX_AVATAR_SZIE = 102400;
var ProfileComponent = (function () {
    function ProfileComponent(auth, userService, route, location) {
        this.auth = auth;
        this.userService = userService;
        this.route = route;
        this.location = location;
        this.user = null;
        this.isReadonly = true;
        this.startDate = null;
        this.isActiveName = '';
        this.isActiveDate = '';
        this.isActiveNick = '';
        this.isActiveName = '';
        this.isActiveDate = '';
        this.isActiveNick = '';
    }
    ProfileComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.forEach(function (params) {
            _this.userService.getUser(params['id']).then(function (user) {
                _this.user = user;
                _this.startDate = _this.dateToString(_this.user.startDate);
                _this.isReadonly = !_this.auth.isCurrentUser(_this.user.id);
            }).catch(function (error) { return alert('get user error: ' + error); });
        });
    };
    ProfileComponent.prototype.goBack = function () {
        this.location.back();
    };
    ProfileComponent.prototype.onBlur = function (owner) {
        var _this = this;
        if (this.isReadonly) {
            return;
        }
        switch (owner) {
            case 'name':
                this.userService.update(this.user.id, { name: this.user.name })
                    .catch(function (error) { return alert('update profile error: ' + error); });
                break;
            case 'startDate':
                this.userService.update(this.user.id, { created_at: this.user.startDate.toISOString() })
                    .catch(function (error) { return alert('update profile error: ' + error); });
                break;
            case 'nickname':
                this.userService.exists(null, this.user.nickname, this.user.id).then(function (result) {
                    console.log(result);
                    if (!result) {
                        _this.userService.update(_this.user.id, { nickname: _this.user.nickname })
                            .catch(function (error) { return alert('update profile error: ' + error); });
                    }
                }).catch(function (error) { return alert('sign up error: ' + error); });
                break;
        }
    };
    ProfileComponent.prototype.onChange = function (event) {
        var _this = this;
        if (this.isReadonly) {
            return;
        }
        var files = event.srcElement.files;
        if (!files) {
            return;
        }
        for (var _i = 0, files_1 = files; _i < files_1.length; _i++) {
            var file = files_1[_i];
            if (file.size >= MAX_AVATAR_SZIE) {
                alert('Avatars size is very large, it must be less than 100kb.');
                continue;
            }
            var reader = new FileReader();
            reader.onload = function (data) {
                var contents = data.target;
                _this.user.avatar = contents.result;
                _this.userService.update(_this.user.id, { picture: _this.user.avatar })
                    .catch(function (error) { alert('update profile error: ' + error); });
            };
            reader.readAsDataURL(file);
        }
        ;
    };
    ProfileComponent.prototype.onAvatarClick = function (input) {
        if (!this.isReadonly) {
            input.click();
        }
    };
    ProfileComponent.prototype.onStartDateChange = function () {
        this.user.startDate = new Date(this.startDate);
    };
    ProfileComponent.prototype.dateToString = function (date) {
        return date.getFullYear() + '-' + ('0' + (date.getMonth() + 1)).slice(-2) + '-' + ('0' + date.getDate()).slice(-2);
    };
    ProfileComponent.prototype.checkActiveClass = function (className) {
        switch (className) {
            case 'name':
                this.isActiveName = 'activeTr';
                break;
            case 'date':
                this.isActiveDate = 'activeTr';
                break;
            case 'nick':
                this.isActiveNick = 'activeTr';
        }
    };
    ProfileComponent.prototype.checkDeactiveClass = function (className) {
        switch (className) {
            case 'name':
                this.isActiveName = '';
                break;
            case 'date':
                this.isActiveDate = '';
                break;
            case 'nick':
                this.isActiveNick = '';
        }
    };
    ProfileComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'profile',
            templateUrl: 'profile.component.html'
        }), 
        __metadata('design:paramtypes', [auth_service_1.Auth, user_service_1.UserService, router_1.ActivatedRoute, common_1.Location])
    ], ProfileComponent);
    return ProfileComponent;
}());
exports.ProfileComponent = ProfileComponent;
//# sourceMappingURL=profile.component.js.map