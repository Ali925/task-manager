"use strict";
var router_1 = require('@angular/router');
var login_component_1 = require('./login/login.component');
var users_component_1 = require('./users/users.component');
var profile_component_1 = require('./profile/profile.component');
var registration_component_1 = require('./registration/registration.component');
var appRoutes = [
    { path: '', component: login_component_1.LoginComponent },
    { path: 'users', component: users_component_1.UsersComponent },
    { path: 'profile/:id', component: profile_component_1.ProfileComponent },
    { path: 'registration', component: registration_component_1.RegistrationComponent },
    { path: '**', redirectTo: '' }
];
exports.appRoutingProviders = [];
exports.routing = router_1.RouterModule.forRoot(appRoutes);
//# sourceMappingURL=app.routes.js.map