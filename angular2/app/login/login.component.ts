import { Component } from '@angular/core';
import { Auth } from '../auth0/auth.service';
import { Router } from '@angular/router';
import { LoadingManager } from '../loading/loadingManager';

@Component({
  moduleId: module.id,
  selector: 'login',
  templateUrl: 'login.template.html'
})

export class LoginComponent extends LoadingManager {
  constructor(private auth: Auth, private router: Router) {
    super(false);
  }

  signIn(email: string, passcode: string): void {
    this.standby();
    this.auth.login(email, passcode, (error => {
      this.ready();

      if (error) {
        alert("something went wrong: " + error.message);
      }
    }));
  }

  signUp(): void {
    this.router.navigate(['/registration']);
  }
};