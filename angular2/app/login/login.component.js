"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var auth_service_1 = require('../auth0/auth.service');
var router_1 = require('@angular/router');
var loadingManager_1 = require('../loading/loadingManager');
var LoginComponent = (function (_super) {
    __extends(LoginComponent, _super);
    function LoginComponent(auth, router) {
        _super.call(this, false);
        this.auth = auth;
        this.router = router;
    }
    LoginComponent.prototype.signIn = function (email, passcode) {
        var _this = this;
        this.standby();
        this.auth.login(email, passcode, (function (error) {
            _this.ready();
            if (error) {
                alert("something went wrong: " + error.message);
            }
        }));
    };
    LoginComponent.prototype.signUp = function () {
        this.router.navigate(['/registration']);
    };
    LoginComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'login',
            templateUrl: 'login.template.html'
        }), 
        __metadata('design:paramtypes', [auth_service_1.Auth, router_1.Router])
    ], LoginComponent);
    return LoginComponent;
}(loadingManager_1.LoadingManager));
exports.LoginComponent = LoginComponent;
;
//# sourceMappingURL=login.component.js.map