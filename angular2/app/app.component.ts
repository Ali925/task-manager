import { Component } from '@angular/core';
import { Auth } from './auth0/auth.service';
import { UserService } from './users/user.service';

@Component({
  selector: 'my-app',
  providers: [Auth, UserService],
  templateUrl: 'app/app.template.html'
})

export class AppComponent {
  constructor(private auth: Auth, private userService: UserService) { }
};