"use strict";
var LoadingManager = (function () {
    function LoadingManager(val) {
        this.loading = val;
    }
    LoadingManager.prototype.standby = function () {
        this.loading = true;
    };
    LoadingManager.prototype.ready = function () {
        this.loading = false;
    };
    return LoadingManager;
}());
exports.LoadingManager = LoadingManager;
//# sourceMappingURL=loadingManager.js.map