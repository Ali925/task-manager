export class LoadingManager {
    public loading: boolean;

    constructor(val: boolean) {
        this.loading = val;
    }

    public standby(): void {
        this.loading = true;
    }

    public ready(): void {
        this.loading = false;
    }
}