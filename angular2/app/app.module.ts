import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule, JsonpModule } from '@angular/http';

import { AUTH_PROVIDERS } from 'angular2-jwt';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { UsersComponent } from './users/users.component';
import { ProfileComponent } from './profile/profile.component';
import { UserService } from './users/user.service';
import { RegistrationComponent } from './registration/registration.component';
import { LoadingComponent } from './loading/loading.component';
import { routing, appRoutingProviders } from './app.routes';

@NgModule({
    declarations: [
        AppComponent,
        RegistrationComponent,
        LoginComponent,
        UsersComponent,
        ProfileComponent,
        LoadingComponent
    ],
    providers: [
        appRoutingProviders,
        UserService,
        AUTH_PROVIDERS
    ],
    imports: [
        BrowserModule,
        routing,
        FormsModule,
        HttpModule,
        JsonpModule
    ],
    bootstrap: [AppComponent],
})
export class AppModule { }