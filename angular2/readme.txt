1) npm install
2) npm install -g wt-cli
3) wt init --container "{ACCOUNT_NAME}" --url "https://sandbox.it.auth0.com" --token "TOKEN" -p "{NAME}"
4) wt create app/webtask.js -p "{NAME}"

Example:
3) wt init --container "testaccountname" --url "https://sandbox.it.auth0.com" --token "eyJhbGciOiJIUzI1NiIsImtpZCI6InVzLTMifQ.eyJqdGkiOiI0ZmUwNjk0ZjAwMmE0ZDYxYmM3MGIwYjQ4OTIwMTUwZiIsImlhdCI6MTQ3ODU3NTQ2MSwiY2EiOltdLCJkZCI6MSwidGVuIjoidGVzdGFjY291bnRuYW1lIn0.0gjsfF-bUnhthGXIDUPMK05okfJeryqjEquzRzmojqc" -p "testaccountname-default"
4) wt create app/webtask.js -p "testaccountname-default"

wt create app/webtask.js -s AUTH0_CLIENT_ID=PbGRlfKRw58GbtDG8WMnJjXxlVDw5SGU -s AUTH0_CLIENT_SECRET=6HvyhyZWxcrKKE9XQS7fYTIKsYvVkoO-XULikJYn9CB9WIzQbCxtp4hLd0SiHnwM -s AUTH0_DOMAIN=testaccountname.auth0.com